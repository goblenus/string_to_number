import random
from ru_number_to_text import num2t4ru
import argparse
import logging
import sys
import itertools

arr_1 = [['ноль'], ['один', 'одна'], ['два', 'две'], ['три'], ['четыре'], ['пять'], ['шесть'], ['семь'], ['восемь'], 
         ['девять']]
arr_2 = ["одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", 
         "восемнадцать", "девятнадцать"]
arr_3_4 = [["десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", 
            "девяносто"], 
           ["сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", 
            "девятьсот"]]
arr_5 = ["тысяч", "миллион", "миллиард", "триллион", "квадриллион", "квинтиллион", "секстилион", "септилион", 
         "окталион"]

def string_to_number(number_string):
    if type(number_string) != str or number_string == "":
        return None

    global arr_1
    global arr_2
    global arr_3_4
    global arr_5

    check_words = number_string.lower().split()

    if check_words[0] == "минус":
        if len(check_words) == 1:
            return None
        check_words = check_words[1:]
        sign = -1
    else:
        sign = 1   

    result = 0
    result_temp = 0
    for word in check_words:
        found = False
        for i, test_number in enumerate(arr_1):
            for test_number_variant in test_number:
                if test_number_variant == word:
                    result_temp += i
                    found = True
                    break

            if found:
                break

        if found:
            continue
        
        for i, test_number in enumerate(arr_2):
            if word == test_number:
                result_temp = result_temp + (i + 1) + 10
                found = True
                break
        
        if found:
            continue

        for i, temp_arr in enumerate(arr_3_4):
            for j, test_number in enumerate(temp_arr):
                if word == test_number:
                    result_temp = result_temp + (j + 1) * (10 ** (i + 1))
                    found = True
                    break
            
            if found:
                break
        
        if found:
            continue

        for i, test_multiplier in enumerate(arr_5):
            if word == test_multiplier or word == "{}{}".format(test_multiplier, 'а') \
                or word == "{}{}".format(test_multiplier, 'ов') or word == "{}{}".format(test_multiplier, 'и'):
                result += result_temp * (1000 ** (i + 1))
                result_temp = 0
                found = True
                break

        if found:
            continue
        
        return None

    return (result + result_temp) * sign


def main():
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--random', action='store_true')
    parser.add_argument('--count', type=int, default=999999999999)
    args = parser.parse_args()

    if args.count <= 0 or args.count > 999999999999:
        logging.error("999999999999 >= count > 0")
        return 1

    logging.info("Random" if args.random else "All")
    try:
        if args.random:
            for i in itertools.count(start=0, step=1):
                number = random.randint(-args.count, args.count)
                result = string_to_number(num2t4ru.num2text(number))
                if number != result:
                    logging.error("Error {} != {}".format(number, result))
                    break
                if not ((i + 1) % 10000):
                    logging.info("Checked: {}".format((i + 1)))
        else:
            for number in range(-args.count, args.count):
                result = string_to_number(num2t4ru.num2text(number))
                if number != result:
                    logging.error("Error {} != {}".format(number, result))
                    break
                if not ((number + 1) % 10000):
                    logging.info("Checked: {}".format((number + 1)))
    except KeyboardInterrupt:
        pass

    logging.info("Finished")
    return 0

if __name__ == "__main__":
    sys.exit(main())
